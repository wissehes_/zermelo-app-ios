#  RoosterApp voor Zermelo
### (english: Schedule App for Zermelo)

## (en) Is an iOS app for viewing/editing your Zermelo schedule.
## (nl) Is een iOS app om je Zermelo rooster te bekijken.


## Screenshots

| Setup | Home  | Blokinfo | Week |
| ----  | ----- | -------- | ---- |
| ![Setup screen](./Screenshots/setup.png) | ![Homescreen](./Screenshots/home.png) | ![Blokinfo](./Screenshots/blokinfo.png) | ![Week screen](./Screenshots/week.png) |

## Namen

- *Rooster voor Zermelo* (niet beschikbaar)
- Zermelo App
- RoosterApp voor Zermelo
- Schedulator (Zermelo)

